#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QRegExp>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stat->hide();
    EviewToMXOPC.insert(2, "INT");
    EviewToMXOPC.insert(3, "DINT");
    EviewToMXOPC.insert(4, "REAL");
    EviewToMXOPC.insert(5, "LREAL");
    EviewToMXOPC.insert(11, "BOOL");
    EviewToMXOPC.insert(18, "UINT");
    EviewToMXOPC.insert(19, "UDINT");
    EviewToMXOPC.insert(32769, "BOOL");
    EviewToFlexible.insert(2, "Short");
    EviewToFlexible.insert(3, "Long");
    EviewToFlexible.insert(4, "Float");
    EviewToFlexible.insert(5, "Double");
    EviewToFlexible.insert(11, "Bool");
    EviewToFlexible.insert(18, "Word");
    EviewToFlexible.insert(19, "Dword");
    EviewToFlexible.insert(32769, "Bool");
    PLCMin.insert(2, -32768);PLCMax.insert(2, 32767);
    //PLCMin.insert(3, -2147483648);PLCMax.insert(3, 2147483647);
    //PLCMin.insert(4, "REAL");PLCMax.insert(4, "REAL");
    //PLCMin.insert(5, "LREAL");PLCMax.insert(5, "LREAL");
    PLCMin.insert(18, 0);PLCMax.insert(18, 65535);
    //PLCMin.insert(19, 0);PLCMax.insert(19, 4294967295);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::toLog(QString txt)
{
    ui->stat->append(txt +"\n");
}

void MainWindow::on_load_clicked()
{
    auto filename = QFileDialog::getOpenFileName(
                this, tr("Выберите файл экспорта из E-view"),
                ui->eviewFile->text(), "Text files (*.txt);;CSV files (*.csv)");
    if(!filename.isNull())
        ui->eviewFile->setText(filename);
    ui->stat->hide();
}

void MainWindow::on_save_clicked()
{
    auto filename = QFileDialog::getSaveFileName(
                this, tr("Укажите имя файла результата"),
                ui->mxopcFile->text(), "CSV files (*.csv)");
    if(!filename.isNull())
        ui->mxopcFile->setText(filename);
}

void MainWindow::on_do_2_clicked()
{
    QFile input(ui->eviewFile->text());
    if(!input.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::critical(
                    this, tr("Ошибка"),
                    tr("Ошибка открытия файла E-view: %1")
                    .arg(input.errorString()));
        return;
    }
    QFile output(ui->mxopcFile->text());
    QString dir = ui->mxopcFile->text().section("/",0,-2);
    QFile outputCC(dir+"/Tags.csv");
    if(!output.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::critical(
                    this, tr("Ошибка"),
                    tr("Ошибка создания файла файла MX OPC: %1")
                    .arg(output.errorString()));
        return;
    }
    if(!outputCC.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::critical(
                    this, tr("Ошибка"),
                    tr("Ошибка создания файла файла WinCCTags: %1")
                    .arg(outputCC.errorString()));
        return;
    }
    int counter = 1;
    int ignored = 0;
    QByteArray out;
    QByteArray outCC;
    // MX OPC Header
    out.append("#MX_DataTags;\n\"LocationPath\",\"Name\",\"Description\","
               "\"Enable\",\"AddressAsString\",\"AccessRights\",\"Simulate\","
               "\"SimulationSignal\",\"GenerateAlarms\",\"MessagePrefix\","
               "\"DigitalAlarmDefinition\",\"LimitAlarmDefinition\","
               "\"DataTypeAsString\",\"SwapWBOrder\",\"ConvertToWord\","
               "\"Monitor\",\"PollRateDefinition\",\"UseConversion\","
               "\"Conversion\",\"MXChange_ID\",\"MXChange_Name\","
               "\"ManualValue\",\"ManualValueAsString\",\"BitOffset\","
               "\"Deadband\",\"LocationType\",\"BitField\",\"BitNumber\","
               "\"BitCount\",\"BitAccess\",\"PXTagNo\",\"PXOffset\"");
    QStringList pollPeriods, pollCustom;
    pollPeriods << "1000" << "2000" << "3000" << "4000" << "5000";
    // MX OPC Tag %1 - name, %2 - Address(string format), %3-type (string format)
    // %4 - bit number (0-15), %5 - PollMethod (String "200ms")
    QString mxOPCTag = "\"\\\\Address Space\\"+ui->deviceName->text()+
            "\\Tags\",\"%1\",\"\","
                       "\"Yes\",\"%2\",\"Read, Write\",\"No\",\"\","
                       "\"No\",\"\",\"\",\"\",\"%3\",\"No\",\"No\",\"Yes\","
                       "\"%5\",\"No\",\"\",,\"\",\"No\",\"%6\",%4,\"0\","
                       "4,\"No\",0,1,\"Yes\",1,0\n";
    QString winCCFlexTag = "%1\t"+ui->connName->text()+"\t"
            +ui->deviceName->text()+".Tags.%1\t%2\t\t1\t2\t%3\t\t\t\t\t"
            +"%5\t%6\t%7\t%8\t%4\t\t0\t\n";
    QRegExp mxBit("([0123456789ABCDEF]{1})$");
    while(!input.atEnd())
    {
        //Active,DataType,Gain,ItemID,Name,Offset,Size,UpdateRate)
        QString tag = QString::fromLocal8Bit(input.readLine()).remove("\n").remove("\n");
        auto props = tag.split(",");
        if(props.size() != 8)
        {
            QMessageBox::critical(
                        this, tr("Ошибка формирования"),
                        tr("Неверное количество свойств %1 в строке №%2."
                           " Процесс прерван!")
                        .arg(props.size())
                        .arg(counter));
            return;
        }
        //Active,DataType,Gain,ItemID,Name,Offset,Size,UpdateRate)
        bool ok;
        bool active = props.first().toInt(&ok) > 0;
        if(!ok || !active)
        {
            toLog(tr("Тег %1 не активен. Пропускаем").arg(props.at(4)));
            continue;
        }
        int type = props.at(1).toInt(&ok);
        if(!ok)
        {
            QMessageBox::critical(
                        this, tr("Ошибка формирования"),
                        tr("Неверный тип тега %1 в строке №%2."
                           " Процесс прерван!")
                        .arg(props.first())
                        .arg(counter));
            return;
        }
        if(EviewToMXOPC.contains(type))
        {
            //Active,DataType,Gain,ItemID,Name,Offset,Size,UpdateRate)
            // MX OPC Tag %1 - name, %2 - Address(string format), %3-type (string format)
            // %4 - bit number (0-15), %5 - PollMethod (String "200ms")
            auto address = props.at(3);
            auto tagname = props.at(4);
            auto gainS = props.at(2);
            auto gain = gainS.replace(",",".").toDouble(&ok);
            auto pollRate = props.at(7);
            if(!pollPeriods.contains(pollRate) && !pollCustom.contains(pollRate))
            {//Если период обновления не найден в MX OPC, добавим
                pollCustom << pollRate;
            }
            pollRate = pollRate+" ms";
            QString mxtype = EviewToMXOPC[type];
            int bitN = 0;
            bool isBool = mxtype.toLower() == "bool";
            if(isBool)
            {
                qDebug() << "BOOL field calculate bitNumber";
                ok = false;
                if(mxBit.indexIn(address) > -1)
                {
                    bitN = mxBit.cap(1).toInt(&ok, 16);
                    qDebug() << "BitNumber field" << mxBit.cap(1) << "number" << bitN;
                    address.replace(mxBit, QString::number(bitN));
                }
                if(!ok)
                {
                    QMessageBox::critical(
                                this, tr("Ошибка формирования"),
                                tr("Неверный адрес бита %1 в строке %3 №%2."
                                   " Процесс прерван!")
                                .arg(mxBit.cap(1))
                                .arg(counter)
                                .arg(address));
                    return;
                }
            }
            out += mxOPCTag.arg(tagname).arg(address).arg(mxtype).arg(bitN)
                    .arg(pollRate).arg(isBool?"False":"0");
            /*
        #Column A: tag name (mandatory - without apostrophe)
        #Column B: connection (if empty then internal)
        #Column C: address (depends on the based connection)
        #Column D: data type (depends on the based connection)
        #Column E: length (necessary if data type is string)
        #Column F: array count
        #Column G: acquisition mode (1 = On Demand 2 = cyclic on use (Default) 3 = cyclic continuous)
        #Column H: acquisition cycle e.g. 1 s
        #Column I: Upper Limit (floating point number)
        #Column J: Additional Upper Limit (floating point number)
        #Column K: Additional Lower Limit (floating point number)
        #Column L: Lower Limit (floating point number)
        #Column M: Linear scaling (0 or false, 1 or true)
        #Column N: Upper PLC scaling value (floating point number)
        #Column O: Lower PLC scaling value (floating point number)
        #Column P: Upper HMI scaling value (floating point number)
        #Column Q: Lower HMI scaling value (floating point number)
        #Column R: start value (type depends on the based data type)
        #Column S: UpdateID
        #Column T: Comment (max. 500 characters)
        (separator is tab, in example replaced ;)
        A   B            C            D   E F G  H     I J K L M  N O  P  Q R S   T
        X0;Connection_1;FX3G.Tags.X0;Bool; ;1;2;100 ms; ; ; ; ;0;10;0;100;0; ;0;123456
            QString winCCFlexTag = "%1\t"+ui->connName->text()+"\t"
                    +ui->deviceName->text()+".Tags.%1\t%2\t\t1\t2\t%3\t\t\t\t\t"
                    +"%5\t%6\t%7\t%8\t%4\t0\t\t";
        %1 - name, %2 - type, %3 -pollRate, %4 = linearScalling enabled*/
            int plcMin = 0, plcMax = 10, hmiMin = 0, hmiMax = 100;
            int linEnabled = gain != 1.0?1:0;
            if(linEnabled)
            {
                if(PLCMin.contains(type))
                {
                //Высчитываем лимиты для ПЛК в зависимости от типа
                    plcMin = PLCMin[type];
                    plcMax = PLCMax[type];
                    hmiMin = static_cast<int>(plcMin*gain);
                    hmiMax = static_cast<int>(plcMax*gain);
                }
                else
                {
                    toLog(tr("Неизвестный тип %1 с коэффициентом! "
                             "Невозможно вычислить лимиты. "
                             "Игнорируем коэффициент! Имя тэга: %2")
                          .arg(EviewToFlexible[type])
                          .arg(tagname));
                            linEnabled = 0;
                }
            }
            auto outCCS = winCCFlexTag.arg(tagname).arg(EviewToFlexible[type])
                    .arg(pollRate).arg(linEnabled).arg(plcMin).arg(plcMax)
                    .arg(hmiMin).arg(hmiMax);
            outCC += outCCS.toUtf8();
        }
        else {
            toLog(tr("Неизвестный тип %1").arg(type));
            ignored++;
        }
        counter++;
    }
    /*#MX_PollRateDefinitions;
    "LocationPath","Name","PrimaryRate","Phase","EnablePrimaryRate","Default"
    "\\Poll Method Definitions","1000ms",1000,0,"Yes","Yes"
    "\\Poll Method Definitions","2000ms",2000,0,"Yes","No"
    "\\Poll Method Definitions","3000ms",3000,0,"Yes","No"
    "\\Poll Method Definitions","4000ms",4000,0,"Yes","No"
    "\\Poll Method Definitions","5000ms",5000,0,"Yes","No"
    "\\Poll Method Definitions","PollMethod100",100,0,"Yes","No"*/
    QByteArray pollPrefix;
    for(auto poll: pollCustom)
    {
        pollPrefix += QString("#MX_PollRateDefinitions;\"LocationPath\",\"Name\","
                      "\"PrimaryRate\",\"Phase\",\"EnablePrimaryRate\","
                      "\"Default\"\n\"\\\\Poll Method Definitions\",\"" + poll
                      +" ms\",1000,0,\"Yes\",\"No\"\n").toUtf8();
    }
    out = pollPrefix+out;
    ui->stat->setText(tr("Обработано строк: %1\n"
                         "Проигнорировано (неизвестный тип): %2\n"
                         "Импортировано тегов: %3").arg(counter).arg(ignored)
                      .arg(counter-ignored));
    ui->stat->show();
    output.write(out);
    outputCC.write(outCC);
    outputCC.close();
    output.close();
}
