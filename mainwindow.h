#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void    toLog(QString);
    void on_load_clicked();

    void on_save_clicked();

    void on_do_2_clicked();

private:
    Ui::MainWindow *ui;
    QMap<int, QString>   EviewToMXOPC;
    QMap<int, QString>   EviewToFlexible;
    QMap<int, int>    PLCMin, PLCMax;
};

#endif // MAINWINDOW_H
